from django.shortcuts import render

def home (request):
	return render(request, 'Web.html')
	
def guestbook (request):
	return render(request, 'Form.html')