from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Rahmadian Tio Pratama' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 6, 3) #TODO Implement this, format (Year, Month, Date)
npm = 1706044074 # TODO Implement this
hobby = "Watching movie"
place = "Fasilkom Universitas Indonesia"
deskripsi = "My name is Rahmadian Tio Pratama, you can call me Tio."

steffi_name="Steffi Alexandra"
steffi_birth_date=date(1999, 7, 2)
steffi_npm = 1706043992
steffi_hobbi="Sleep, eat, playing game"
steffi_deskripsi="Dia teman kuliah gw"

wibi_name="Hermawan Wibisana"
wibi_birth_date=date(1999, 5, 26)
wibi_npm = 1706043405
wibi_hobbi="playing basketball, eat, playing game"
wibi_deskripsi="Dia teman kuliah gw"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'Place' : place,
                'Hobby' : hobby, 'Description' : deskripsi, "Steffi_name" : steffi_name, "Steffi_age" : calculate_age(steffi_birth_date.year)
        , "Steffi_npm" : steffi_npm, "Steffi_Hobby": steffi_hobbi, "Steffi_deskripsi" : steffi_deskripsi,
                "wibi_name": wibi_name, "wibi_age": calculate_age(wibi_birth_date.year), "wibi_npm": wibi_npm,
                "wibi_Hobby": wibi_hobbi, "wibi_deskripsi": wibi_deskripsi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
